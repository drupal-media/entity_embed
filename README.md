# Entity Embed Module

Allows any entity to be embedded using a WYSIWYG and text format.

### Embed by UUID (recommended):
```
<div data-entity-type="node" data-entity-uuid="07bf3a2e-1941-4a44-9b02-2d1d7a41ec0e" data-view-mode="teaser" />
```

### Embed by ID (not recommended):
```
<div data-entity-type="node" data-entity-id="1" data-view-mode="teaser" />
```
